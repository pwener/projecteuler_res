'''
Created on 30/06/2013

@author: Phelps

2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

'''

def edivisivel(intervalo):
    numero = 1
    x = 2
    continua = True
    while continua:
        if numero % x == 0:
            if x == intervalo:
                continua = False
            else:
                x += 1
        else:
            numero += 1
            x = 2
            
    return numero
    
print(edivisivel(20))