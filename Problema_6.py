'''
Created on 29/07/2013

@author: phelps

The sum of the squares of the first ten natural numbers is,

1² + 2² + ... + 10² = 385
The square of the sum of the first ten natural numbers is,

(1 + 2 + ... + 10)² = 552² = 3025
Hence the difference between the sum of the squares of the 
first ten natural numbers and the square of the sum is 3025 − 385 = 2640.
Find the difference between the sum of the squares of the first one hundred 
natural numbers and the square of the sum.

'''

def sumsquare(lim):
    soma = 0
    for x in range(1, lim+1):
        soma += x**2

    return soma

def squaresum(lim):
    soma = 0
    for x in range(1, lim+1):
        soma += x
    
    return soma**2

print(squaresum(100)-sumsquare(100))