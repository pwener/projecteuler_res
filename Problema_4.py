'''
Created on 02/04/2013

@author: Phelps

A palindromic number reads the same both ways.
The largest palindrome made from the product of two 2-digit numbers is 9009 = 91x99.
Find the largest palindrome made from the product of two 3-digit numbers.
'''

limInf = 100
limSup = 999
maiorPalindromico = 0
termos = [0,0]

for y in range(limInf, limSup):
    for x in range(limInf, limSup):
        Nproduto = y * x
        produto = str(Nproduto)
        if Nproduto > maiorPalindromico and produto == produto[::-1]:
            maiorPalindromico = Nproduto
            termos[0] = x
            termos[1] = y

print("O maior palindromico é" , maiorPalindromico)
print('Produto de %d X %d' %(termos[0], termos[1]))