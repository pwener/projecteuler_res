'''

The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:

13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.

'''

maiorFator = 0
ng = 0

def numeroCadeias(fator):
    n = 1
    resultado = 0
    while resultado != 1:
        if fator % 2 == 0:
            resultado = fator / 2
        else:
            resultado = 3*fator + 1
        fator = int(resultado)
        n += 1
    return n

maiornumerofatores = 0

for x in range(1, int(1e6)):
    b = numeroCadeias(x)
    if b > maiornumerofatores:
        maiornumerofatores = x

print(maiornumerofatores)
