from math import sqrt

def primo(x):
    for n in range(2, int(sqrt(x)+1)):
        if x % n == 0:
            return False
    return True

soma = 0

for x in range(2, int(1e6)):
    if primo(x):
        soma+= x
        
print(soma)
