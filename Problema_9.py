'''

A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

a² + b² = c²
For example, 3² + 4² = 9 + 16 = 25 = 5².

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.

'''

from math import sqrt

def pythagoreanTriplet(soma, mostrarTodos = False):
    for x in range(1, 1000):
        for y in range(1, x):
            for z in range(1, 1000):
                if x ** 2 + y **2 == z ** 2:
                    somaTotal = x+y+z
                    if mostrarTodos:
                        print("x = %s \t| y = %s\t | z = %s " % (x, y, z))
                    if somaTotal == soma:
                        return x*y*z


         
print(pythagoreanTriplet(1000, True))




