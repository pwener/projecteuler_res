'''
Created on 09/08/2013

@author: phelps

Work out the first ten digits of the sum of the following one-hundred 50-digit numbers.

Escreva os dez primeiros numeros da soma dos seguintes 100 numeros de 50 digitos

numeros no arquivo "numerodoazar"

'''
import time

start = time.time()

arquivo = open('arquivos/numerodoazar', 'r')
numeros = arquivo.read()

intervalo = 0
qt_numeros = 0
lista_Numeros = []
while qt_numeros < 100:
    lista_Numeros.append(numeros[intervalo:intervalo+50])
    intervalo += 51
    qt_numeros += 1

z = 0

for x in lista_Numeros:
    lista_Numeros[z] = int(x)
    z += 1

resultado = str(sum(lista_Numeros))

end = time.time()

tempo = end - start

print("Resultado: %s\nTempo: %s"%(resultado[:10], tempo))
