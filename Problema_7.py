'''
Created on 29/07/2013

@author: phelps

By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?
'''

def primo(posicao):
    ocorrencia = 1
    numero = 3
    while ocorrencia != posicao:
        for x in range(2, numero):
            if numero % x != 0:
                if x == numero-1:
                    ocorrencia += 1
                    numero += 1
                    break
                continue
            else:
                numero += 1
                break
            
    return numero - 1

print(primo(10001))